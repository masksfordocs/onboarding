# DevOps - Onboarding Guide

## Sections

- [Introduction](#introduction)
- [Teammates Who Can Help](#teammates-who-can-help)
- [Prerequisites](#prerequisites)
- [Required Accounts](#required-accounts)
- [Required Software](#required-software)

## Introduction

This guide is for new CI/CD team members. Use this guide to load up your laptop with all of the tools, workspaces and permissions you will need to help automate the development, testing and deployment processes. Bring work to the people (not people to the work)!

## Teammates Who Can Help

Our fearless leader is `@Ariana Bray (she/her) - Seattle` ("Ari") and our core team includes:
- `@Tehut (she/her)  - Seattle`
- `@Peter Loron - DevOps`
- `@Cory Parent`
- `@pete nuwayser DC he/his`

## Prerequisites

1. All accounts and software as specified in the [Common](./Common.md) role.

1. Contact Ari Bray on Slack for a video recording of a recent onboarding session, where she and Pete walk through the additional software repos needed to build the environment.

## Required Accounts

1. Additional access will be provided during an onboarding session that we will schedule with you. 

## Required Software

- [ansible][ansible_url]
- [awscli][awscli_url]
- [packer][packer_url]
- [terraform][tf_url]

[ansible_url]: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

[awscli_url]: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html

[packer_url]: https://www.packer.io/intro/getting-started/install.html

[tf_url]: https://www.terraform.io/downloads.html
