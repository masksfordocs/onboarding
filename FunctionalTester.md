# Functional Testing - Onboarding Guide

## Sections

- [Introduction](#introduction)
- [Teammates Who Can Help](#teammates-who-can-help)
- [Prerequisites](#prerequisites)
- [Required Software](#required-software)
- [Required Accounts](#required-accounts)
- [Other Resources Needed](#other-resources-needed)

## Introduction

This guide is for new Functional Testing team members. Use this guide to load up your laptop with all of the tools, workspaces and permissions you will need to help automate the development, testing and deployment processes. Bring work to the people (not people to the work)!

## Teammates Who Can Help

The Functional Testing Lead is Holly Proctor (Winston-Salem GMT-4)

## Prerequisites

1. All accounts and software as specified in the [Common](./Common.md) role.

## Required Software

- [Cypress][cypress_url]

> **TO_DO** Holly to bullet out other software needed for testing

- Gatling?

- Selenium?

- SOAP UI?

- what else?

## Required Accounts

## Other Resources Needed

[cypress_url]: https://docs.cypress.io/guides/getting-started/installing-cypress.html#System-requirements