# Developer - Onboarding Guide

## Sections

- [Introduction](#introduction)
- [Prerequisites](#prerequisites)
- [Other Resources Needed](#other-resources-needed)

## Introduction

This guide is for front-end and back-end developers. Use this guide to load up your laptop with all of the tools, workspaces and permissions you will need to float between FE and BE work as needed. 

## Prerequisites

1. Review all items in [Common](./Common.md), including Goals/Purposes/Values, Dev Workflow, Platform Roadmap, Teams and Leads, 

1. The back end is sourced from [Distributed Aid][DA_url]. Back End Developers should review the Distributed Aid [CONTRIBUTING][DA_CONTRIBUTING_url] document. You also need to review and agree to their Code of Conduct.

1. All accounts and software as specified in the [Common](./Common.md) role

1. Review the [Milestone 1 needs list][m1_needs_list_url].

1. Review the backlog in the [Issues Board][issues_board_url].

<!-- ## Required Accounts

## Required Software

## Other Resources Needed
 -->
## Document Maintainer

The maintainer of this document is **Pete Nuwayser** (Slack: `@pete nuwayser DC he/his`) (DC GMT-4). Pete can help get you started and pointed in the right direction.

[DA_CONTRIBUTING_url]: https://www.notion.so/distributeaid/CONTRIBUTING-4ba5060441e44925b5e22aa674d4729e
[DA_url]: https://distributeaid.org/
[issues_board_url]: https://gitlab.com/masksfordocs/toolbox/-/boards
[toolbox_repo_url]: https://gitlab.com/masksfordocs/toolbox