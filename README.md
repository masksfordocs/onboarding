# README.md

## Sections

- [Introduction](#Introduction)
- [Order Of Onboarding](#order-of-onboarding)
- [Our Ask](#our-ask)
- [Document Maintainer](#document-maintainer)

## Introduction

Welcome to the Masks For Docs Logistics Supplies Platform Team! If you need help getting started with contributing to the Logistics Supplies Platform, this is the place to start. This guide should serve to get people onboard until we can automate some of the account creation and developer environment setup.

## Order Of Onboarding

1. First, if you are just arriving, please [join us on Slack][slack_join_url] if you have not done so already. Head over to #developers and introduce yourself, then fill out the [developer intake survey][intake_form_url] so we can learn about you and the work you would like to do. Don't forget to include your name!

1. After you make contact, please visit the [Common](./Common.md) page. This is where you can learn about our goals, purpose and values; become familiar with the architecture and documentation, and connect to the source code.

1. When you have completed the Common tasks, please proceed to one of the pages below that applies to your specific role. Note that Front end and Back end developers could be asked to help with one or the other, hence a single onboarding guide.

    - [Developer (FE & BE)](./Developer.md)
    - [DevOps Engineer](./DevOps.md)
    - [Functional Tester](./FunctionalTester.md) - page in progress
    <!-- - [Test Automator](./TestAutomator.md) - to be built -->
    <!-- - [508 Tester](./508.md) -->
    <!-- - [Performance Tester](./PerfTester.md)  -->
    <!-- - [Platform Tester](./PlatTester.md) -->
    <!-- - [Test Data Manager](./TDM.md) -->


    Each page includes the names of people who can help you. 

## Our Ask

If you see something in this documentation that needs fixing or correcting, please take a moment to submit a merge request with your correction. Whether it's a one-letter typo or a larger update to how we do things, it's very important that we all work together to make this process faster, easier and better for future team members. If you have questions, please ask within GitLab or ping Pete Nuwayser on Slack.

## Document Maintainer

The maintainer of this document is **Pete Nuwayser** `pete nuwayser (DC) he/his` (GMT-4). Pete can help you get found.

[intake_form_url]: https://forms.gle/8QB2FNcY1dGzE3bD8
[slack_join_url]: https://slack-invite.masksfordocs.com
